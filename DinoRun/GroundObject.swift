//
//  GroundObject.swift
//  DinoRun
//
//  Created by Vikas Koul on 4/5/17.
//  Copyright © 2017 Vikas Koul. All rights reserved.
//

import Foundation
import SpriteKit

class GroundObject: SKSpriteNode{
   
    let segNumber = 20
    static let color_base = UIColor(red:255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    static let color_sec = UIColor(red:139.0/255.0, green: 175.0/255.0, blue: 200.0/255.0, alpha: 1.0)

    init(size:CGSize) {
        super.init(texture: nil, color: UIColor.init(colorLiteralRed:159.0/255.0, green: 201.0/255.0, blue: 250.0/255.0, alpha: 1.0), size: CGSize(width:size.width+2,height:size.height))
        anchorPoint = CGPoint(x: 0, y: 0.5)
        for var i in (0..<segNumber){
            var segColor: UIColor!
            if i % 2 == 0{
                segColor = GroundObject.color_base
            }
            else{
                segColor = GroundObject.color_sec
            }
            let widthCalc = Float(self.size.width)/Float(segNumber)
            print(widthCalc)
            let segment = SKSpriteNode(color: segColor, size:CGSize(width:CGFloat(widthCalc),height:self.size.height))
            segment.anchorPoint = CGPoint(x: 0.0, y: 0.5);
            segment.position = CGPoint(x: CGFloat(i)*segment.size.width, y: 0.0)
            addChild(segment)
            
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func groundMove(){
        let adjustDuration = TimeInterval(frame.size.width / kmoveValue)
        let moveLeft = SKAction.moveBy(x: -frame.size.width/2, y: 0.0, duration: adjustDuration/2)
        let resetGround = SKAction.moveTo(x: 0, duration: 0)
        let repeatSequence = SKAction.sequence([moveLeft,resetGround])
        run(SKAction.repeatForever(repeatSequence))
    }
}
