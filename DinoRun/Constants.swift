//
//  Constants.swift
//  DinoRun
//
//  Created by Vikas Koul on 18/04/17.
//  Copyright © 2017 Vikas Koul. All rights reserved.
//

import Foundation
import UIKit

let kgroundHeight: CGFloat = 20.0
let kextraHeightReduction: CGFloat = 50.0
let kmoveValue:  CGFloat = 320.0
