//
//  CloudGenerator.swift
//  DinoRun
//
//  Created by Vikas Koul on 18/04/17.
//  Copyright © 2017 Vikas Koul. All rights reserved.
//

import Foundation
import SpriteKit

class CloudGenerator : SKSpriteNode{
    let c_width: CGFloat = 100.0
    let c_height:CGFloat = 50.0
    
    var generateTimer : Timer!
    func cloudGeneratorFunc(num:Int){
        for var i in (0..<num){
            let cloud = CloudObj(size: CGSize(width: c_width, height: c_height))
            let x = CGFloat(arc4random_uniform(UInt32(size.width))) - size.width/2
            let y = CGFloat(arc4random_uniform(UInt32(size.height))) - size.height/2
            cloud.position = CGPoint(x: x, y: y)
            cloud.zPosition = -1
            addChild(cloud)
        }
    }
    
    func generateAtTimeGiven(sec: TimeInterval){
        generateTimer = Timer.scheduledTimer(timeInterval: sec, target: self, selector: "generateCloud", userInfo: nil, repeats: true)
    }
    
    func generateCloud(){
        let x = size.width/2 + c_width/2
        let y = CGFloat(arc4random_uniform(UInt32(size.height))) - size.height/2
        let cloud = CloudObj(size:CGSize(width: c_width, height: c_height))
        cloud.position = CGPoint(x: x, y: y)
        cloud.zPosition = -1
        addChild(cloud)
    }
    
}
