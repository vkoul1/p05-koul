//
//  CactusObjGenerator.swift
//  DinoRun
//
//  Created by Vikas Koul on 20/04/17.
//  Copyright © 2017 Vikas Koul. All rights reserved.
//

import Foundation
import SpriteKit

class CactusObjGanarator:SKSpriteNode{
    var generateTimer : Timer?
    func startGeneration(sec: TimeInterval){
        generateTimer = Timer.scheduledTimer(timeInterval: sec, target: self, selector: "generateBlock", userInfo: nil, repeats: true)
    }
    
    func generateBlock(){
        
        var scale:CGFloat
        let blocker = CactusObj()
        let rand = arc4random_uniform(2)
        if rand == 0{
            scale = -1.0
        }
        else{
            scale = 1.0
        }
    //    let translate = SKAction.moveBy(x: size.width/2 + blocker.size.width/2, y: scale*(kgroundHeight/2 + blocker.size.height/2 + 30.0) , duration: 0.1)
    //    let flip = SKAction.scaleY(to: scale, duration: 0.1)
    //    run(translate)
     //   run(flip)
        blocker.position = CGPoint(x: size.width/2 + blocker.size.width/2, y: scale*(kgroundHeight/2 + blocker.size.height/2))
        addChild(blocker)
        
    }
}
