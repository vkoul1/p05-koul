//
//  CloudObj.swift
//  DinoRun
//
//  Created by Vikas Koul on 18/04/17.
//  Copyright © 2017 Vikas Koul. All rights reserved.
//

import Foundation
import SpriteKit

class CloudObj:SKShapeNode{
    init(size: CGSize){
        super.init()
        let path = UIBezierPath(ovalIn: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        fillColor = UIColor.white
        self.path = path.cgPath
    }
    func moveCloud(){
       let moveLeft = SKAction.moveBy(x: -10.0, y: 0, duration: 1)
       run(SKAction.repeatForever(moveLeft))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
