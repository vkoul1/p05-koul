//
//  GameScene.swift
//  DinoRun
//
//  Created by Vikas Koul on 3/27/17.
//  Copyright © 2017 Vikas Koul. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    var ground : GroundObject!
    var hero : SidHero!
    var cloudG : CloudGenerator!
    var blockerG : CactusObjGanarator!
    
    var hasGameStarted = false
    
    override func didMove(to view: SKView) {
       
        backgroundColor = UIColor(red:129.0/255.0, green: 155.0/255.0, blue: 180.0/255.0, alpha: 1.0)
        let bgTexture = SKTexture(imageNamed: "bg.png")
        let backgroundImage = SKSpriteNode(texture: bgTexture, size: view.frame.size)
        backgroundImage.position = view.center
        backgroundImage.zPosition = -1
        addChild(backgroundImage)
 
        //Creating the ground
        ground = GroundObject(size: CGSize(width:view.frame.width,height: kgroundHeight))
        ground.position = CGPoint(x: 0.0, y: view.frame.size.height/2)
        addChild(ground)
        
        //Creating the hero player
        hero = SidHero()
        hero.position = CGPoint(x: 70.0, y: ((ground.frame.size.height/2) + ground.position.y + hero.frame.size.height) - kextraHeightReduction)
        addChild(hero)
        
        //Adding clouds on the sky
        cloudG = CloudGenerator(color:UIColor.clear,size: view.frame.size)
        cloudG.position = view.center
        addChild(cloudG)
        cloudG.cloudGeneratorFunc(num: 7)
        cloudG.generateAtTimeGiven(sec: 7)
        
        //Adding blocks
        blockerG = CactusObjGanarator(color:UIColor.clear,size: view.frame.size)
        blockerG.position = view.center
        addChild(blockerG)
        
    }
    func start(){
        hasGameStarted = true
        //Making the ground move on touch
        ground.groundMove();
        blockerG.startGeneration(sec: 3)
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if !hasGameStarted{
            start()
        }
        else{
            //Making the flipping for the hero
            hero.flipMove()
        }
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
}
