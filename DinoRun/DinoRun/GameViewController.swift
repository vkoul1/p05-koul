//
//  GameViewController.swift
//  DinoRun
//
//  Created by Vikas Koul on 3/27/17.
//  Copyright © 2017 Vikas Koul. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

class GameViewController: UIViewController {

    var scene: GameScene!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let skview = view as! SKView
        skview.isMultipleTouchEnabled = true
        
        scene = GameScene(size:skview.bounds.size)
        scene.scaleMode = .aspectFill
        skview.presentScene(scene)
    }

    override var shouldAutorotate: Bool {
        return false
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
}
