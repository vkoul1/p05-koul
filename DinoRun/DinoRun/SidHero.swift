//
//  SidHero.swift
//  DinoRun
//
//  Created by Vikas Koul on 06/04/17.
//  Copyright © 2017 Vikas Koul. All rights reserved.
//

import Foundation
import SpriteKit

class SidHero: SKSpriteNode{
    var body: SKSpriteNode!
    var flipChk = false
    
    init(){
        super.init(texture: nil, color: UIColor.clear, size: CGSize(width:50,height:75))
        
        body = SKSpriteNode(imageNamed: "sid1.ico")
        body.size = CGSize(width:50,height:75)
        addChild(body)

    }
    func flipMove(){
        flipChk = !flipChk
        var scale:CGFloat!
        if flipChk{
            scale = -1.0
        }
        else{
            scale = 1.0
        }
        let translate = SKAction.moveBy(x: 0, y: scale*(size.height + kgroundHeight - 25.0), duration: 0.1)
        let flip = SKAction.scaleY(to: scale, duration: 0.1)
        run(translate)
        run(flip)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
