//
//  CactusObj.swift
//  DinoRun
//
//  Created by Vikas Koul on 20/04/17.
//  Copyright © 2017 Vikas Koul. All rights reserved.
//

import Foundation
import SpriteKit

class CactusObj:SKSpriteNode{
    let cactusWidth = 30.0
    let cactusheight = 50.0
    var cactusBody: SKSpriteNode!
    
    init(){
        super.init(texture:nil,color:UIColor.clear,size:CGSize(width: cactusWidth, height: cactusheight))
        cactusBody = SKSpriteNode(imageNamed: "cactus.png")
        cactusBody.size = CGSize(width:cactusWidth,height:cactusheight)
        addChild(cactusBody)
        moveCactus()
    }
    func moveCactus(){
        let moveLeft = SKAction.moveBy(x: -kmoveValue, y: 0, duration: 1)
        run(SKAction.repeatForever(moveLeft))
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
